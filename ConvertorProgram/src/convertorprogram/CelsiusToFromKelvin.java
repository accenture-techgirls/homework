package convertorprogram;

import java.util.Scanner;

public class CelsiusToFromKelvin {

public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter temperature Celsius :");
		double celsius = Double.parseDouble(scanner.nextLine());
		double kelvin = celsius + 273.15;
		System.out.println(celsius + "�C = " + kelvin + " K");
		
		System.out.println("Enter temperature Kelvin :");
		double kelvin1 = Double.parseDouble(scanner.nextLine());
		double celsius1 = kelvin1 - 273.15;
		System.out.println(kelvin1 + " K = " + celsius1 + "�C");
		scanner.close();
		


	}
}
