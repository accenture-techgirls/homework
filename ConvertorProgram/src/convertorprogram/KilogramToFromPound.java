package convertorprogram;

import java.util.Scanner;

public class KilogramToFromPound {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter weight Kilogram :");
		double kilogram = Double.parseDouble(scanner.nextLine());
		double pound= kilogram * 2.2046;
		System.out.println(kilogram + " kg = " + pound + " lb");
		
		System.out.println("Enter weight Pound :");
		double pound1 = Double.parseDouble(scanner.nextLine());
		double kilogram1 = pound1 / 2.2046;
		System.out.println(pound1 + " lb = " + kilogram1 + " kg");
		scanner.close();
		


	}
}
