package convertorprogram;

import java.util.Scanner;

public class SquareMeterToFromSquareKilometer {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter area Square meter :");
		double squaremeters = Double.parseDouble(scanner.nextLine());
		double squarekilometer= squaremeters / 1000000;
		System.out.println(squaremeters + " m� = " + squarekilometer + " km�");
		
		System.out.println("Enter area Square Kilometer :");
		double squarekilometer1 = Double.parseDouble(scanner.nextLine());
		double squaremeters1 = squarekilometer1 * 1000000;
		System.out.println(squarekilometer1 + " km� = " + squaremeters1 + " m�");
		scanner.close();
		


	}
}

