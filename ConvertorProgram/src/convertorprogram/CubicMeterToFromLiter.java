package convertorprogram;

import java.util.Scanner;

public class CubicMeterToFromLiter {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter volume Cubic meter :");
		double cubicmeter = Double.parseDouble(scanner.nextLine());
		double liter= cubicmeter * 1000;
		System.out.println(cubicmeter + " m� = " + liter + " L");
		
		System.out.println("Enter volume Liter :");
		double liter1 = Double.parseDouble(scanner.nextLine());
		double cubicmeter1 = liter1 / 1000;
		System.out.println(liter1 + " L = " + cubicmeter1 + " m�");
		scanner.close();
		


	}
}