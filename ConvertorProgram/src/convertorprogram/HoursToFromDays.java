package convertorprogram;

import java.util.Scanner;

public class HoursToFromDays {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter time Hours :");
		double hours = Double.parseDouble(scanner.nextLine());
		double days = hours / 24;
		System.out.println(hours + " Hours = " + days + " Days");
		
		System.out.println("Enter time Days :");
		double days1 = Double.parseDouble(scanner.nextLine());
		double hours1 = days1 * 24;
		System.out.println(days1 + " Days = " + hours1 + " Hours");
		scanner.close();
		


	}
}
