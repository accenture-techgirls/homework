package convertorprogram;

import java.util.Scanner;

public class CelsiusToFromFahrenheit {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter temperature Celsius :");
		double celsius = Double.parseDouble(scanner.nextLine());
		double fahrenheit = celsius * 9 / 5 + 32;
		System.out.println(celsius + "�C = " + fahrenheit + "�F");
		
		System.out.println("Enter temperature Fahrenheit :");
		double fahrenheit1 = Double.parseDouble(scanner.nextLine());
		double celsius1 = (fahrenheit1 - 32) * 5 / 9;
		System.out.println(fahrenheit1 + "�F = " + celsius1 + "�C");
		scanner.close();
		


	}
}
