package convertorprogram;

import java.util.Scanner;

public class MetersToFromFeet {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter distance Meters :");
		double meters = Double.parseDouble(scanner.nextLine());
		double feet= meters / 0.3048;
		System.out.println(meters + " m = " + feet + " ft");
		
		System.out.println("Enter volume Feet :");
		double feet1 = Double.parseDouble(scanner.nextLine());
		double meters1 = feet1 * 0.3048;
		System.out.println(feet1 + " ft = " + meters1 + " m");
		scanner.close();
		


	}
}
