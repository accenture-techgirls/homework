package convertorprogram;

import java.util.Scanner;

public class ConvertsUSDtofromEUR {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter amount USD :");
		double usd = Double.parseDouble(scanner.nextLine());
		double euro = usd / 1.21590;
		System.out.println(usd + " USD = " + euro + " EUR");
		
		System.out.println("Enter amount EUR :");
		double euro1 = Double.parseDouble(scanner.nextLine());
		double usd1 = euro1 * 1.21590;
		System.out.println(euro1 + " EUR = " + usd1 + " USD");
		scanner.close();
		


	}
}