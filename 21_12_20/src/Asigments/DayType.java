package Asigments;

import java.util.Scanner;

public class DayType {

	public static void main(String[] args) {
		
		int dayNumber;
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Please enter day number :");
		dayNumber = Integer.parseInt(myScanner.nextLine());
		
		if (dayNumber <= 0 || dayNumber>7) {
			System.out.println("Day type is incorrect");
			return;
		}
		if (dayNumber == 6 || dayNumber == 7 )
			System.out.println("It is holiday");
		else 
			System.out.println("It is a working day");
	}

}
