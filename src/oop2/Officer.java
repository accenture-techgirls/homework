package oop2;

public class Officer  extends  Person {

	private int officerID, crimesSolved;

	public Officer() {
		// TODO Auto-generated constructor stub
	}

	public Officer(String name, String surname, String workingDistrict, int officerID, int crimesSolved) {
		super(name,surname);
		this.crimesSolved = crimesSolved;
		this.officerID =officerID;
	}			
		
	public int getOfficerID() {
		return officerID;
	}

	public void setOfficerID(int officerID) {
		this.officerID = officerID;
	}

	public int getCrimesSolved() {
		return crimesSolved;
	}

	public void setCrimesSolved(int crimesSolved) {
		this.crimesSolved = crimesSolved;
	}

	

	@Override
	public String toString() {
		return super.toString() + System.lineSeparator()
		+ "Officer ID: " + this.officerID + System.lineSeparator() + "Crimes Solved: "
		+ this.crimesSolved; 
		
	}


}

