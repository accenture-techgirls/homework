package oop2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class District {

	private String title, city;
	private int districtID;
	private ArrayList<Person> peronsInTheDistrict = new ArrayList<Person>();


	///If the arguments are defined for the constructor, we cannot create the object, without providing them
	public District(String title, String city, int districtID) {
		this.city = city;
		this.districtID = districtID;
		this.title = title;
		this.city = city;
	}
	public ArrayList<Person> getPeronsInTheDistrict() {
		return peronsInTheDistrict;
	}

	public void setPeronsInTheDistrict(ArrayList<Person> peronsInTheDistrict) {
		this.peronsInTheDistrict = peronsInTheDistrict;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getDistrictID() {
		return districtID;
	}

	public void setDistrictID(int districtID) {
		this.districtID = districtID;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "The title:" + this.title + System.lineSeparator() + "City: " + this.city + System.lineSeparator()
				+ "District ID: " + this.districtID + System.lineSeparator() + "There are "
				+ this.peronsInTheDistrict.size() + " people in the district";
	}

	public void addNewPerson(Person person) {
		this.peronsInTheDistrict.add(person);//// add new element to the array list
	}

	public void removerPerson(Person person) {
		this.peronsInTheDistrict.remove(person); ///// remove the corresponding person from the list
	}
	 
	public float calculateAvgLevellInDistrict() {
		float levelSum = 0 , numberOfOfficers = 0;
		Iterator<Person> people= this.peronsInTheDistrict.iterator();
		while (people.hasNext()) {
			Person person = people.next();
			if (person instanceof Officer) {// return true if Person is Officer, returns false otherwise
				Officer officer = (Officer) person; /// here we cast Person to Officer
				numberOfOfficers++;/// we found the officer, so we can increment the total number of the
										/// officers
				levelSum += officer.getCrimesSolved();
			}
		}
		

		return levelSum /  numberOfOfficers;
	}



}
