package oop2;

import java.util.ArrayList;
import java.util.Iterator;

public class Execution {
	
	private static ArrayList<District> peronsInTheDistrict;

	public static void main(String[] args) {
		//Create seven Officers as the objects, 
		//two Districts as the objects and 
		//three Lawyers as the objects.
		Officer[] officers = new Officer[7];
		Lawyer[] lawyers = new Lawyer[3];
		
		for (int i = 0; i < 7; i++)
			officers[i] = new Officer();
			officers[0].setCrimesSolved(40);
			officers[1].setCrimesSolved(25);
			officers[2].setCrimesSolved(32);
			officers[3].setCrimesSolved(30);
			officers[4].setCrimesSolved(31);
			officers[5].setCrimesSolved(23);
			officers[6].setCrimesSolved(35);
		
		for (int i = 0; i < 3; i++)
			lawyers[i] = new Lawyer();
		
		District district1 = new District("District51", "New-York", 12);
		District district2 = new District("District21", "Washington", 22);
		
	
		for (int i = 0; i < 3; i++)
			district1.addNewPerson(officers[i]);
		
		for (int i = 0; i < 2; i++)
			district1.addNewPerson(lawyers[i]);
		
		
		///Add three Officers in the first District and others in the second District.

		for (int i = 3; i < 7; i++)
			district2.addNewPerson(officers[i]);
		for (int i = 2; i < 3; i++)
			district2.addNewPerson(lawyers[i]);

		System.out.println(district1);
		System.out.print(System.lineSeparator());
		System.out.println(district2);
		System.out.print(System.lineSeparator());

		System.out.println("Average level in the first district: " + district1.calculateAvgLevellInDistrict());
		System.out.println("Average level in the second district: " + district2.calculateAvgLevellInDistrict());
		System.out.print(System.lineSeparator());
		
		ArrayList<District> districtStorage = new ArrayList<District>();
		districtStorage.add(district1);
		districtStorage.add(district2); 
		
		District DistracthighestAmountOfPersons = null;
		Iterator<District> iterator = districtStorage.iterator();
		while (iterator.hasNext()) {
			District curentDistrict = iterator.next();
			if (DistracthighestAmountOfPersons == null ||
				DistracthighestAmountOfPersons.getPeronsInTheDistrict().size() < curentDistrict.getPeronsInTheDistrict().size()){
				DistracthighestAmountOfPersons = curentDistrict;
			
			}
		}

		System.out.println(DistracthighestAmountOfPersons.getTitle() + " is distract  with the highest amount of Persons.");

	}

}

