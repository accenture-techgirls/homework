package oop2;

public class Lawyer extends Person  {

	private int lawyerID, helpedinCrimeSolving ;
	public int getHelpedinCrimeSolving;
	
	public Lawyer() {
		// TODO Auto-generated constructor stub
	}
	
	public Lawyer(String name, String surname, int lawyerID, int helpedinCrimeSolving) {
		super(name,surname);
		this.lawyerID = lawyerID;
		this.helpedinCrimeSolving = helpedinCrimeSolving;
	}
	
	public int getLawyerID() {
		return lawyerID;
	}

	public void setLawyerID(int lawyerID) {
		this.lawyerID = lawyerID;
	}

	public int getHelpedinCrimeSolving() {
		return helpedinCrimeSolving;
	}

	public void setHelpedinCrimeSolving(int helpedinCrimeSolving) {
		this.helpedinCrimeSolving = helpedinCrimeSolving;
	}

	@Override
	public String toString() {
		return super.toString() +System.lineSeparator() +
		"lawyer ID: " + this.lawyerID + System.lineSeparator() +
		"Help in crime solving: " + this.helpedinCrimeSolving;
	}

}
		

	


