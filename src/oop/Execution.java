package oop;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jdk.dynalink.beans.StaticClass;

public class Execution {
	

	public static void main(String[] args) {
		//Create seven Officers as the objects, 
		//two Districts as the objects and 
		//three Lawyers as the objects.
		Officer[] officers = new Officer[7];
		
		for (int i = 0; i < 7; i++)
			officers[i] = new Officer();
		
		Lawyer lawyer1 = new Lawyer("James", "White", 2348,5);
		Lawyer lawyer2 = new Lawyer("William", "Right", 2346,2);
		Lawyer lawyer3 = new Lawyer("Noah", "Time", 2342,1);

		District district1 = new District("District51", "New-York", 12);
		District district2 = new District("District21", "Washington", 22);
		
		
		for (int i = 0; i < 3; i++)
			district1.addNewOfficer(officers[i]);
		
		///Add three Officers in the first District and others in the second District.

		for (int i = 0; i < 3; i++)
			district1.addNewOfficer(officers[i]);

		for (int i = 3; i < 7; i++)
			district2.addNewOfficer(officers[i]);

		System.out.println(district1);
		System.out.print(System.lineSeparator());
		System.out.println(district2);
		System.out.print(System.lineSeparator());
		System.out.println(lawyer1);
		System.out.print(System.lineSeparator());
		System.out.println(lawyer2);
		System.out.print(System.lineSeparator());
		System.out.println(lawyer3);
		System.out.print(System.lineSeparator());
		//Create an array list for Lawyers storing. Put all Lawyers in it
		List<Lawyer> allLawyers = new ArrayList<Lawyer>();
		Lawyer lawyers  = new Lawyer();
		allLawyers.add(lawyer1);
		allLawyers.add(lawyer2);
		allLawyers.add(lawyer3);
	
		//Find out which Lawyer has helped the most to solve crimes.
		Lawyer bestLawyer = null;
		Iterator<Lawyer> iterator = allLawyers.iterator();
		while(iterator.hasNext()) {
			Lawyer curentLawyer = iterator.next();
			if (bestLawyer == null ||
				bestLawyer.getHelpedinCrimeSolving() < curentLawyer.getHelpedinCrimeSolving()){
				
				bestLawyer = curentLawyer;
			
			}
		}
		System.out.println(bestLawyer.getName() + " " + bestLawyer.getSurname() +  " has helped the most to solve crimes.");
	
		System.out.println(lawyers.totalNumberOfCrimeSolving() + "of the crimes in which solving "  );
	
				 
	}

}

