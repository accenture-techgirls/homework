package oop;

import java.util.ArrayList;

public class District {

	private String title, city;
	private int districtID;
//	private Officer[] officersInTheDistrict; we can't change the size of the array
	private ArrayList<Officer> officersInTheDistrict = new ArrayList<Officer>();//// this is the object, where we can
																				//// store as many officer as we want

	///If the arguments are defined for the cinstructor, we cannot create the object, without providing them
	public District(String title, String city, int districtID) {
		this.city = city;
		this.districtID = districtID;
		this.title = title;
		this.city = city;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getDistrictID() {
		return districtID;
	}

	public void setDistrictID(int districtID) {
		this.districtID = districtID;
	}

	@Override
	public String toString() {
		return "The title:" + this.title + System.lineSeparator() + "City: " + this.city + System.lineSeparator()
				+ "District ID: " + this.districtID + System.lineSeparator() + "There are "
				+ this.officersInTheDistrict.size() + " officers in the district";
	}

	public void addNewOfficer(Officer officer) {
		this.officersInTheDistrict.add(officer);//// add new element to the array list
	}

	public void removerOfficer(Officer officer) {
		this.officersInTheDistrict.remove(officer); ///// remove the corresponding officer from the list
	}

	public float calculateAvgLevellInDistrict() {
		float levelSum = 0;
		/// method size will return the number of the elements in the collection
		for (int i = 0; i < this.officersInTheDistrict.size(); i++)
			levelSum += this.officersInTheDistrict.get(i).calculateLevel();

		return levelSum / this.officersInTheDistrict.size();
	}

	public ArrayList<Officer> getOfficersInTheDistrict() {
		return officersInTheDistrict;
	}

	public void setOfficersInTheDistrict(ArrayList<Officer> officersInTheDistrict) {
		this.officersInTheDistrict = officersInTheDistrict;
	}

}
