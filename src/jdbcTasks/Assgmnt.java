package jdbcTasks;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Assgmnt {

	private static Connection conn;

	public static void main(String[] args) throws Exception {

		Class.forName("com.mysql.jdbc.Driver");
		Assgmnt.conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/", "root", "");
		fillCategories();
		//fillSuppliers();
		fillStatus();
	}

	private static void fillCategories() throws Exception {
		Statement statement = Assgmnt.conn.createStatement();
		statement.execute("INSERT INTO java13.categories (name) VALUES ('portable')");
		statement.execute("INSERT INTO java13.categories (name) VALUES ('computers')");
		statement.execute("INSERT INTO java13.categories (name) VALUES ('PCs')");
		statement.execute("INSERT INTO java13.categories (name) VALUES ('software')");
		statement.execute("INSERT INTO java13.categories (name) VALUES ('accessories')");
	}

	private static void fillSuppliers() throws Exception {
		Statement statement = Assgmnt.conn.createStatement();
		statement.execute(
				"INSERT INTO java13.suppliers (name,phone_number,email) VALUES ('Supplier1','+371254755','supplier1@gmail.com')");
		statement.execute(
				"INSERT INTO java13.suppliers (name,phone_number,email) VALUES ('Supplier2','+371254775','supplier2@gmail.com')");
		statement.execute(
				"INSERT INTO java13.suppliers (name,phone_number,email) VALUES ('Supplier3','+371235775','supplier3@gmail.com')");

	}

	private static void fillStatus() throws Exception {
		Statement statement = Assgmnt.conn.createStatement();
		statement.execute("INSERT INTO java13.status (name) VALUES ('entered')");
		statement.execute("INSERT INTO java13.status (name) VALUES ('in processing')");
		statement.execute("INSERT INTO java13.status (name) VALUES ('canceled')");
		statement.execute("INSERT INTO java13.status (name) VALUES ('delivered')");
	}

}
