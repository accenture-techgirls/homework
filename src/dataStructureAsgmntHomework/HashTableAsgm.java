package dataStructureAsgmntHomework;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class HashTableAsgm {
	public static void main (String[] arg) {
		
		 Map<String, Double> store = new Hashtable<String, Double>();
		 store.put("map",0.12);
		 store.put("chair",12.54);
		 store.put("shelf",5.25);
		 store.put("closet",250.99);
		 store.put("box",16.87);
		 store.put("bed",320.15);
		 store.put("lamp",12.35);
		 store.put("sofa",450.52);
		 
		 int lessThanOne = 0;
		 
		 Iterator<Entry<String, Double>> storeIterator = store.entrySet().iterator(); 
		 while (storeIterator.hasNext()) { 
	         Map.Entry mapElement = (Map.Entry)storeIterator.next(); 
	         if((double)mapElement.getValue() < 1)
	        	 lessThanOne += 1;
	         
		 }
		 System.out.println(lessThanOne +  " products price are under 1 eur.");
	     System.out.print(System.lineSeparator());
	     
		 String minPrice = store.entrySet().stream().min(Map.Entry.comparingByValue()).get().getKey();
		 
		 System.out.println("Product " + minPrice + " price is the lowest.");
		 System.out.print(System.lineSeparator());
		 
		 String maxValue = store.entrySet().stream().max(Map.Entry.comparingByValue()).get().getKey();
		 System.out.println("Product " + maxValue + " price is the largest.");
		 System.out.print(System.lineSeparator());
		 
		 Map<String, Double> store2 = new Hashtable<String, Double>();
		 store2.put("pen",0.40);
		 store2.put("notebook",3.20);
		 
		 store.putAll(store2);
		 
		 System.out.println(store);
		}
		 	 
	}
